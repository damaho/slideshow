/* *********************************************************************
 * 
 * slideshower - coverflow themed viewer for jquery
 * 
 * Copyright (C) 2007-2014 Dave Horn
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ********************************************************************* */
(function($){
	function getPrefix(prop){
		var prefixes = ['Moz','Webkit','Khtml','O','ms'],
		elem     = document.createElement('div'),
		upper      = prop.charAt(0).toUpperCase() + prop.slice(1),
		pref     = "",
		len      = 0;

		for (len = prefixes.length; len--;) {
			if((prefixes[len] + upper) in elem.style){
				pref = (prefixes[len]);
			}
		}

		if (prop in elem.style) {
			pref = (prop);
		}
		return pref;
	}

	var vendorPrefix = getPrefix('transform');

	$.widget("ui.slideshower", {
		options: {
			ui_settings: {}, // use this to override the id, css or class name of elements
			max_viewed: 7,    // this will determine how max overall are shown (so for 7, there's 3 left, 3 right, plus the main one - even numbers favor the left side)
			browse_mode: true, // if true, clicking on a slide navigates to it
			hide_captions: true,
			start_id: null,
			minimum_margin_top: 0,
			minimum_margin_bottom: 0,
			square_size: 0,
			images: [],
			fade: true
		},
		
		current_id : null, 
		
		start: function() {
			this.setPlayingItemById(this.options.start_id);
			var play_id_first = this.options.start_id;
			if (this.items.length > 0 && play_id_first == null) {
				play_id_first = $(this.items[Math.floor(this.items.length / 2)]).prop('id');
				this.setPlayingItemById(play_id_first); // if nothing else set, start in the middle
			}
			this.resetSize();
		},
		
		_create: function() {
			var self = this;
			
			this.current_playing = null;
			this.items = [];
			this.item_map = {};
			this.initUiSettings();
			
			$.each(this.options.ui_settings, function(setting, data) {
				if (self.ui_settings[setting] != undefined) {
					if (data.id != undefined)
						self.ui_settings[setting].id = data.id;
					if (data.css != undefined)
						self.ui_settings[setting].css = data.css;
					if (data.class != undefined)
						self.ui_settings[setting].class = data.class;
				}
			});
			
			this.initUI();
			
			this.initElementData();
			
			$(window).resize(function() {
				self.resetSize();
			});
			
			$.each(this.options.images, function(key, value) {
				self.addNewImage('images' + key, value);
			});
		},
		
		refresh: function() {
			this.detectItems();
		},
		
		select: function(id) {
			//console.log('select');
			if (typeof this.item_map[id] == 'undefined')
				return false;
				
			var from_id = this.current_playing,
				distance = this.item_map[from_id] - this.item_map[id],
				max_scale = 1, 
				side='left', 
				p_start_z = 0, p_end_z, 
				item_z, item_id, 
				css = {},
				current_position = this.getElementById(id).position().left,
				center_position,
				selected_idx = this.item_map[id],
				max_left = 0,
				max_right = 0,
				selected = false,
				self = this;
			
			if (this.options.max_viewed > 0) {
				max_left = Math.floor(parseInt(this.options.max_viewed) / 2),
				max_right = parseInt(this.options.max_viewed) - max_left - 1;
			}
			
			this.setSqareSize();
			
			center_position = ($(this.element).width() / 2) - (this.options.square_size / 2);
			
			p_end_z = self.items.length;
			
			$.each(this.items, function(idx, element) {
				var item_id = $(element).attr('id'), 
					idx_diff = 0, abs_diff = 0,
					scale = (max_scale * .5), 
					img_opacity = self.options.browse_mode ? .4: .2, 
					bg_opacity = self.options.browse_mode ? .6: .4;
				
				if (typeof self.item_map[item_id] == 'undefined')
					return false;
				//$(element).html($(element).prop('id'));
				idx_diff = self.item_map[item_id] - selected_idx;
				abs_diff = Math.abs(idx_diff);
				
				item_z = p_end_z - abs_diff;
				// exact match
				if (idx_diff == 0) {
					side = 'right';
					scale = max_scale;
					img_opacity = 1;
					bg_opacity = 1;
					selected = $(element);
				}
				// border match
				if (abs_diff == 1) {
					scale = max_scale * .75;
					img_opacity = self.options.browse_mode ? .6: .4;
					bg_opacity = self.options.browse_mode ? .7: .6;
				}
				
				if (self.options.max_viewed > 0) {
					if (idx_diff < (max_left * -1) || idx_diff > max_right) {
						bg_opacity = 0;  // leave img opacity alone
					}
				}
				
				if (!self.options.fade) {
					img_opacity = 1;
					bg_opacity = 1;
				}
				
				$(element).find('img').css('opacity', img_opacity);
				
				css['zIndex'] = item_z;
				css['opacity'] = bg_opacity;
				css[vendorPrefix + 'Transform'] = 'scale(' + scale + ',' + scale + ') translate3d(0,0,0)';
				css['transform'] = 'scale(' + scale + ',' + scale + ') translate3d(0,0,0)';
				css['left'] = (center_position + ((idx - selected_idx)) * (self.options.square_size / 2)) + 'px';
				$(element).css(css);
			});
			
			this.setPlayingItemById(id);
			return selected;
		},
		
		getElementById: function(id) {
			//console.log('getElementById');
			return $(this.element).find('#' + id);
		},
		
		getElementsByClass: function(classname) {
			//console.log('getElementByClass');
			return $(this.element).find('.' + classname);
		},
		
		getItemByIndex: function(index) {
			//console.log('getItemByIndex');
			if (typeof this.items[index] == 'undefined')
				return false;
			return $(this.items[index]);
		},
		
		getItemById: function(id) {
			//console.log('getItemById');
			if (this.item_map[id])
				return false;
			return this.getItemByIndex(this.item_map[id]);
		},
		
		getItems: function() {
			return this.items;
		},
		
		getItemMap: function() {
			return this.item_map;
		},
		
		getConfig: function() {
			return this.current_playing;
		},
		
		detectItems: function() {
			//console.log('detectItems');
			var self = this,
				wrapper = this.getElementById(self.ui_settings.container.id);
			this.items = $("> *", wrapper);
			this.item_map = {};
			$.each(this.items, function(idx, element) {
				self.item_map[$(element).attr('id')] = idx;
			});
		},
		
		move: function(how_many, allow_jump) {
			//console.log('move');
			if (typeof this.item_map[this.current_playing] == 'undefined') {
				return false;
			}
			
			var current_index = this.item_map[this.current_playing],
				new_index = current_index + how_many;
			
			if (allow_jump) {
				if (new_index < 0)
					new_index = this.items.length - Math.abs(new_index % this.items.length);
				else
					new_index = new_index % this.items.length;
			}
			
			if (typeof this.items[new_index] == 'undefined')  // still? how is that possible?
				return false;
			
			return this.skipTo($(this.items[new_index]).prop('id'));
		},
		
		setPlayingItemById: function(id) {
			if (typeof this.item_map[id] == 'undefined')
				return false;
			this.current_playing = id;
		},
		
		setSqareSize: function() {
			this.options.square_size = parseInt(this.ui_settings.clone_slidecontainer.css.height);
		},
		
		setSenderThumb: function(p_url) {
			var img_sender_img = this.getElementById(this.ui_settings.user_data_left.id).find('img');
			if (p_url == null) {
				img_sender_img.hide();
				return false;
			}
			img_sender_img.prop('src', p_url);
			img_sender_img.show();
		},
		
		setCaption: function (caption) {
			//console.log('setCaption');
			this.getElementById(this.ui_settings.user_data_caption.id).html(caption);
		},

		setSender: function (sender) {
			//console.log('setSender');
			this.getElementById(this.ui_settings.user_data_id.id).html(sender);
		},
		
		skipTo: function(id) {
			//console.log('skipTo');
			var selected_item = this.select(id);
			if (selected_item === false)
				return false;
			var img = $(this.items[this.item_map[id]]).find('img');
			
			this.setSender(img.prop('data-user'));
			this.setCaption(img.prop('data-caption'));
			this.setPlayingItemById(id);
			return selected_item;
		},
		
		skipToCurrent: function() {
			if (this.current_playing == null)
				return false;
			return this.skipTo(this.current_playing);
		},
		
		cutOne: function(element) {
			//console.log('cutOne');
			element.remove();
			this.detectItems();
			return element;
		},

		fitImage: function(item) {
			var theImage = new Image();
			theImage.src = $(item).attr("src");
			var img_height = parseInt(theImage.height),
				img_width = parseInt(theImage.width),
				div_height = parseInt($(item).parent().height()),
				div_width = parseInt($(item).parent().width());
			
			if (img_height >= img_width) {
				if (img_height < div_height && img_height > 0) {
					var ratio = div_height / img_height
						margin = (div_width - (ratio * img_width)) / 2 + 'px';
					$(item).css({'width': 'auto', 'height': '100%', 'margin-left': margin});
				} else if (img_height > div_height) {
					$(item).css({'width': 'auto', 'height': '100%'});
					var img_width = $(item).width();
					var div_width = $(item).parent().width();
					var newMargin = (div_width-img_width) / 2 + 'px';
					$(item).css({'margin-left': newMargin});
				}
			} else {
				if (img_width < div_width && img_width > 0) {
					var ratio = div_width / img_width
						margin = (div_height - (ratio * img_height)) / 2 + 'px';
					$(item).css({'height': 'auto', 'width': '100%', 'margin-top': margin});
				} else if (img_width > div_width) {
					$(item).css({'height': 'auto', 'width': '100%'});
					var img_height = $(item).height();
					var div_height = $(item).parent().height();
					var newMargin = (div_height-img_height) / 2 + 'px';
					$(item).css({'margin-top': newMargin});
				}
			}
		},
		
		_insertItem: function(element, p_side) {
			var self = this;
			
			if (this.item_map[$(element).prop('id')] != undefined) {
				return false;
			}
			
			if (p_side == 'left') {
				element.css('left', (-1 * this.options.square_size) + 'px');
				this.getElementById(this.ui_settings.container.id).prepend(element);
			} else {
				element.css('left', (this.items.length * (this.options.square_size + parseInt($('.slideplatform').css('margin-left'))) + 'px'));
				this.getElementById(this.ui_settings.container.id).append(element);
			}
			this.detectItems();
			return element;
		},
		
		_removeItem: function(element) {
			$(element).remove();
			this.detectItems();
		},
		
		addNewImage: function(id, img, p_side) {
			var self = this,
				obj = this.getElementById(this.ui_settings.clone_slidecontainer.id).clone(true),
				new_img;
			
			if (p_side == undefined)
				p_side = 'right';
			
			obj.prop('id', id);
			if (this.options.browse_mode)
				obj.click(function() { self.select(id); });
			
			
			
			if (img) {
				var data_caption, data_user;
				
				if (typeof img == 'string')
					img = $('<img>').prop({'src': img, 'alt': ''});
				
				data_caption = img.prop('data-caption');
				data_user = img.prop('data-user');
				
				new_img = obj.find('img').prop({'id': 'images' + id, 'src': img.prop('src'), 'alt': id, 'data-user': (data_user == undefined ? '': data_user), 'data-caption': (data_caption == undefined ? '': data_caption)});
				new_img.bind('load', function() { self.fitImage(this); });
				
				//self.fitImage(new_img);  // TODO: This doesn't work quite right yet
			}
			this._insertItem(obj);
		},
		
		addItem: function(id, item) {
			var self = this,
				obj = this.getElementById(this.ui_settings.clone_slidecontainer.id).clone(true);

			if (item == null || item == undefined) {
				id = id.replace(' ', '_').toLowerCase();
				item = $('<div>').css('text-align', 'center').html(id);
			}
			if (typeof item == 'string')
				item = $('<div>').css('text-align', 'center').html(item);

			obj.prop('id', id);
			if (this.options.browse_mode)
				obj.click(function() { self.select(id); });
			
			obj.empty();
			obj.append($(item));
			
			this._insertItem(obj);
		},
		
		removeItemById: function(id) {
			this._removeItem(this.getElementById(id));
		},
		
		resetSize: function() {
			// some values for control
			var self = this, 
				ui_settings = this.ui_settings, 
				default_size = 612, // expected 'normal' size of slide (based on an instagram photo)
				minimum_margin_top = this.options.minimum_margin_top, 
				excess = 0;
			// some values for current settings
			var current_pad_top = parseInt(this.getElementById(ui_settings.inner_wrapper.id).css('padding-top')),
				current_pad_bottom = parseInt(this.getElementById(ui_settings.inner_wrapper.id).css('padding-bottom')),
				current_userdata_height = parseInt(this.getElementById(ui_settings.user_data_wrapper.id).height()),
				current_bottom_margin = current_userdata_height < this.options.minimum_margin_bottom ? this.options.minimum_margin_bottom : current_userdata_height,
				current_slider_margin = parseInt(this.getElementById(ui_settings.container.id).css('margin-top')),
				current_element_height = parseInt($(this.element).height()), 
				current_minimum_slide_size = parseInt(ui_settings.clone_slidecontainer.css['min-height']),
				current_window_height = current_element_height; //parseInt($(window).height());
			
			if (current_minimum_slide_size + current_bottom_margin + current_pad_top + 2 > current_window_height)
				current_minimum_slide_size = current_window_height - current_bottom_margin - current_pad_top - 2;
				
			if (minimum_margin_top < current_pad_bottom)
				minimum_margin_top = current_pad_bottom;
			// targets
			var minimum_useful_overall_height = (current_bottom_margin + current_pad_top + current_minimum_slide_size),
				current_target_height = current_window_height;
			
			var maximum_viewing_height = current_target_height - current_bottom_margin - current_pad_top;
			var new_inner_wrapper_height = current_target_height - (current_pad_top + current_pad_bottom),
				new_slide_size = maximum_viewing_height, 
				new_slide_container_margin = current_slider_margin >= minimum_margin_top ? current_slider_margin : minimum_margin_top;
			
			if (new_slide_size + new_slide_container_margin > maximum_viewing_height) {
				excess = (new_slide_size + new_slide_container_margin) - maximum_viewing_height;
				if (new_slide_size > default_size) {
					if (new_slide_size - default_size > excess) {
						new_slide_size -= excess;
					} else {
						new_slide_size = default_size;
					}
					excess = (new_slide_size + new_slide_container_margin) - maximum_viewing_height;
				}
				
				if (excess > 0 && new_slide_container_margin > minimum_margin_top) {
					if (new_slide_container_margin - minimum_margin_top > excess) {
						new_slide_container_margin -= excess;
					} else {
						new_slide_container_margin = minimum_margin_top;
					}
					excess = (new_slide_size + new_slide_container_margin) - maximum_viewing_height;
				}
				
				if (excess > 0) {
					new_slide_size -= excess;
				}
			}
			
			
			this.getElementById(ui_settings.inner_wrapper.id).css('height', new_inner_wrapper_height + 'px');
			this.getElementById(ui_settings.container.id).css({'margin-top': new_slide_container_margin + 'px'});
			this.getElementsByClass(ui_settings.clone_slidecontainer.class).css(
				{
					'height': new_slide_size + 'px', 
					'min-height': current_minimum_slide_size + 'px', 
					'min-width': current_minimum_slide_size + 'px', 
					'width': new_slide_size + 'px'
				});

			//$.each(this.items, function(key, container) {
			//	self.fitImage($(container).find('img'));  // TODO - not really working yet
			//});
			
			this.ui_settings.clone_slidecontainer.css.height = new_slide_size + 'px';
			
			this.skipToCurrent();
		},
		
		initElementData: function() {
			//console.log('initElementData');
			this.setSqareSize();
		},
		
		initUiSettings: function() {
			this.ui_settings = {
				outer_wrapper: {
					id: 'coverflow_page_wrapper',
					css: {
						position: 'relative',
						margin: '0 auto',
						width: '100%',
						height: '100%'
					}
				},
				inner_wrapper: {
					id: 'coverflow_wrapper',
					css: {
						width: '100%',
						padding: '10px 0px 10px 0px',
						overflow: 'hidden',
						position: 'relative'
					}
				},
				container: {
					id: 'coverflow',
					css: {
						position: 'relative',
						margin: '0px',
						display: 'inline-block',
						height: '100%',
						width: '2600px',
						'white-space': 'nowrap',
						'padding': '0 3%',
						'position': 'absolute',
						'top': '0px',
						'left': '0px',
						'margin-top': '0px',
						'transition': 'left .2s ease-out'
					}
				},
				clone_wrapper: {
					id: 'clone_pallette',
					css: {
						display: 'none'
					}
				},
				clone_slide: {
					id: 'cf_clone',
					css: {
						position: 'relative'
					}
				},
				clone_slidecontainer: {
					id: 'sp_clone',
					class: 'slideplatform',
					css: {
						position: 'absolute',
						left: '-612px',
						transition: 'all .8s ease-out, z-index .1s linear, opacity .2s linear',
						opacity: '0',
						overflow: 'hidden',
						height: '612px',
						'min-height': '140px',
						'min-width': '140px'
					}
				},
				user_data_wrapper: {
					id: 'user_data_wrapper',
					css: {
						position: 'absolute',
						bottom: '0px',
						height: '150px',
						'max-height': '150px',
						width: '100%',
						overflow: 'hidden',
						display: 'table',
						'table-layout': 'fixed',
						'z-index': '899',
						'opacity': '1'
					}
				},
				user_data_left: {
					id: 'user_data_left',
					css: {
						display: 'table-cell',
						width: '96px',
						padding: '5px 5px 15px 15px',
						'vertical-align': 'top'
					}
				},
				user_data_right: {
					id: 'user_data_right',
					css: {
						'display': 'table-cell',
						'text-align': 'center',
						'vertical-align': 'top',
						'position': 'relative'
					}
				},
				user_data_image: {
					id: 'user_data_image',
					css: {
						height: '96px',
						width: '96px'
					}
				},
				user_data_id: {
					id: 'user_data_id',
					css: {
						color: '#fff',
						display: 'block',
						'font-weight': 'bold',
						'font-size': '12px',
						'text-align': 'center'
					}
				},
				user_data_caption: {
					id: 'user_data_caption',
					css: {
						position: 'relative',
						height: '100%',
						margin: '10px auto 0',
						width: '90%',
						color: '#fff',
						display: 'block',
						'font-size': '1.4em',
						'text-align': 'center',
						overflow: 'hidden',
						'word-wrap': 'break-word'
					}
				}
			};
		},
		
		initUI: function() {
			$(this.element).css('overflow', 'hidden');
			if (this.options.browse_mode)
				this.ui_settings.clone_slidecontainer.css['cursor']  = 'pointer';
			
			if (this.options.hide_captions) {
				this.ui_settings.user_data_wrapper.css['height'] = '0px';
				this.ui_settings.user_data_wrapper.css['display'] = 'none';
			}
			
			//console.log('initUI');
			var ui_settings = this.ui_settings,
				outer_wrapper = $('<div>').prop('id', ui_settings.outer_wrapper.id).css(ui_settings.outer_wrapper.css);
				
				
			// display wrapper
			outer_wrapper.append(
				$('<div>').prop('id', ui_settings.inner_wrapper.id).css(ui_settings.inner_wrapper.css).append(
					$('<div>').prop('id', ui_settings.container.id).css(ui_settings.container.css)
				)
			);
			
			// user data
			outer_wrapper.append(
				$('<div>').prop('id', ui_settings.user_data_wrapper.id).css(ui_settings.user_data_wrapper.css)
					.append($('<div>').prop('id', ui_settings.user_data_left.id).css(ui_settings.user_data_left.css)
						.append($('<img>').prop({'id': ui_settings.user_data_image.id, 'src': '', 'alt': ''}).css(ui_settings.user_data_image.css))
						.append($('<div>').prop('id', ui_settings.user_data_id.id).css(ui_settings.user_data_id.css))
					)
					.append(
						$('<div>').prop('id', ui_settings.user_data_right.id).css(ui_settings.user_data_right.css)
							.append($('<div>').prop('id', ui_settings.user_data_caption.id).css(ui_settings.user_data_caption.css))
					)
			);
			
			// slides are constantly cloned (we could just create them instead of cloning if we wanted)
			outer_wrapper.append(
				$('<div>').prop('id', ui_settings.clone_wrapper.id).css(ui_settings.clone_wrapper.css).addClass(ui_settings.clone_wrapper.class)
					.append($('<div>').prop('id', ui_settings.clone_slidecontainer.id).css(ui_settings.clone_slidecontainer.css).addClass(ui_settings.clone_slidecontainer.class)
						.append($('<img>').prop('id', ui_settings.clone_slide.id).css(ui_settings.clone_slide.css))
					)
			);
			
			$(this.element).append(outer_wrapper);
			this.resetSize();
		}

	});
})($);
