<!doctype html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title></title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script type='text/javascript' src='ui.slideshower.js'></script>
	<style>
		html, body {
			margin: 0;
			padding: 0;
		}

		.slideplatform {
			border: 1px solid #323232;
			background-color: #f0f0f0;
		}
		
		.tester {
			height: 96px;
			border-top: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
		}
		
		.third {
			position: relative;
			height: 600px;
			width: 800px;
			margin-left: auto;
			margin-right: auto;
			border: 1px solid #323232;
		}
		
	</style>
</head>

<body>
Some images (same one over and over actually)
<div id='some_images' class='tester'>
</div>
<br />
Some text
<div id='some_text' class='tester'>
</div>
<br />
Some text2
<div id='some_text2' class='tester'>
</div>
<br />
Some random slides...
<div id='some_moretext' class='third'>
</div>

<script type='text/javascript'>

	// Set up the first example...
	$('#some_images').slideshower({max_viewed: 0});
	for (var i = 0; i < 50; i++) {
		//$('#some_images').slideshower('addNewImage', 'test' + i, 'http://www.groundzeromedia.org/wp-content/uploads/2013/01/ghost_mine.jpg');
		$('#some_images').slideshower('addNewImage', 'testimage' + i, 'http://www.partyhasher.com/images/testpic.jpg');
	}
	$('#some_images').slideshower('start');


	// Set up the second example...
	$('#some_text').slideshower({max_viewed: 0});
	for (var i = 0; i < 50; i++) {
		$('#some_text').slideshower('addItem', 'testtext' + i, 'Some Text ' + i);
	}
	$('#some_text').slideshower('start');
	

	// Set up the second example...
	$('#some_text2').slideshower({max_viewed: 7});
	for (var i = 0; i < 50; i++) {
		$('#some_text2').slideshower('addItem', 'Xtest' + i, 'SSSS ' + i);
	}
	$('#some_text2').slideshower('start');

	// Set up the third example...
	$('#some_moretext').slideshower({max_viewed: 11});
	for (var i = 0; i < 20; i++) {
		//$('#some_moretext').slideshower('addItem', 'moretest' + i, 'Some More Text ' + i);
		$('#some_moretext').slideshower('addNewImage', 'three' + i, 'http://www.partyhasher.com/images/testpic.jpg');
	}
	$('#some_moretext').slideshower('start');
	
	console.log($('#some_images').slideshower('getConfig'));
	console.log($('#some_text').slideshower('getConfig'));
	console.log($('#some_text2').slideshower('getConfig'));
	console.log($('#some_moretext').slideshower('getConfig'));
	
</script>
<body>
</html>
